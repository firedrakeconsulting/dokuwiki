class dokuwiki::selinux {

  file { "/var/www/html/dokuwiki/data":
    ensure   => directory,
    recurse  => true,
    owner    => "apache",
    group    => "apache",
    selinux_ignore_defaults => true,
    seltype  => 'httpd_sys_rw_content_t',
  }
  
   file { "/var/www/html/dokuwiki/conf":
    ensure   => directory,
    recurse  => true,
    owner    => "apache",
    group    => "apache",
    selinux_ignore_defaults => true,
    seltype  => 'httpd_sys_rw_content_t',
  }
  
}
