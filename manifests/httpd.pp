class dokuwiki::httpd {
  package { 'httpd':
    ensure => installed,
  }
  
  service { 'httpd':
    ensure => running,
    enable => true,
    require => Package["httpd"],
  }
  
  file { "/etc/httpd/conf.d/dokuwiki.conf":
        mode => "0644",
        owner => 'root',
        group => 'root',
        source => 'puppet:///modules/dokuwiki/dokuwiki.conf',
  }
  
  file { '/var/www/html/app-gw-probe.htm':
      content => "Application Gateway Probe - Do not remove",
  }
  
  package { 'php70w':
    ensure => installed,
  }
  
  package { 'php70w-opcache':
    ensure => installed,
  }
}
